import java.awt.Color;

import javax.swing.JFrame;



public class MazeRunner {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		JFrame window = new JFrame();
		window.setSize(1024,768);
		window.setTitle("MazeRunner Tester");
		UserInterface request = new UserInterface();
		GraphicGenerator graphics = new GraphicGenerator(request);
		
		window.getContentPane().add(request);
		window.getContentPane().add(graphics);		
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		
		
		while(true){
			graphics.move();
			graphics.repaint();
			Thread.sleep(120);
		}
		
	}

}
