import java.util.*;

public class PointPaths{
	private ArrayList<Point> paths; 
	private Point point;
	public PointPaths(Point p, ArrayList<Point> paths) {
		point = p;
		this.setPaths(paths);
	}
	public ArrayList<Point> getPaths() {
		return paths;
	}
	public void setPaths(ArrayList<Point> paths) {
		this.paths = paths;
	}
	
	public Point getPoint(){
		return point;
	}
	
	
	
}