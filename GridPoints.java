
import java.util.*;

public class GridPoints {
	//private ArrayList<ArrayList<Point>> pointLib;
	private int row;
	private int column;
	//private ArrayList<ArrayList<>>
	/**
	 * 
	 * @param nRow - number of row
	 * @param nCol - number of column
	 */
	public GridPoints(int nRow, int nCol){
		row = nRow;
		column = nCol;
	}
	
	public ArrayList<ArrayList<PointPaths>> getMap(){
		ArrayList<ArrayList<Point>> pointLib = new ArrayList<ArrayList<Point>>();
		ArrayList<ArrayList<PointPaths>> pathLib = new ArrayList<ArrayList<PointPaths>>();
		
		
		int negInf = -Integer.MAX_VALUE;
		for(int r = 0; r < row; ++r){
			ArrayList<Point> hori = new ArrayList<Point>();
			for(int c = 0; c < column; ++c){
				////system.out.println("it is working");
				Point p = new Point(r,c);
				////system.out.println("Creating: " + p.toString());
				hori.add(p);
			}
			pointLib.add(r,hori);
			////system.out.println("r =" + r);
			
		}
		
		
		Boolean n = false;
		Boolean e = false;
		Boolean s = false;
		Boolean w = false;
		Point nextPoint; //neighbor point
		Point staticPoint; //the current point
		Point invalidAccess = new Point(negInf,negInf); //giving trash as an invalidAccess
		ArrayList<Point> paths;
		PointPaths obj;
		/*
		//Test
		for(int r = 0; r < row; ++r){
			for(int c = 0; c < column; ++c){
				nextPoint = pointLib.get(r).get(c);
				//system.out.println("Testing coordinate = "+ nextPoint.toString());
			}
		}
		*/
		
		for(int r = 0; r < row; r++){
			ArrayList<PointPaths> pathHori = new ArrayList<PointPaths>();
			for(int c = 0; c < column; c++){
				//system.out.println("hmmm");
				
				paths= new ArrayList<Point>();
				staticPoint = pointLib.get(r).get(c);
				//Frequently update n, e, s, w to be false for conditions purposes
				n = false;
				e = false;
				s = false;
				w = false;
				//system.out.println("Current position: " + staticPoint.toString());
				//north computation
				if(r > 0){
					nextPoint = pointLib.get(r-1).get(c);
					paths.add(nextPoint);
					n = true; //state it as true if the available move exist
					//system.out.println("North validMove: " + nextPoint.toString());
				}
				
				//checking if north has a valid move, otherwise initialize negative infinity as an invalid move
				if(n == false){
					paths.add(invalidAccess);
					//system.out.println("North validMove: " + invalidAccess.toString());
				}
				
				//east computation
				if(c < column - 1){
					nextPoint = pointLib.get(r).get(c+1);
					paths.add(nextPoint);
					e = true;
					//system.out.println("East validMove: " + nextPoint.toString());
				}
				
				if(e == false){
					//invalidAccess = new Point(negInf,negInf);
					paths.add(invalidAccess);
					//system.out.println("East validMove: " + invalidAccess.toString());
				}
				
				//south computation
				if(r < row -1){
					nextPoint = pointLib.get(r+1).get(c);
					paths.add(nextPoint);
					s = true;
					//system.out.println("South validMove: " + nextPoint.toString());
				}
//				
				if(s == false){
					//invalidAccess = new Point(negInf,negInf);
					paths.add(invalidAccess);
					//system.out.println("South validMove: " + invalidAccess.toString());
				}
				
				//west computation
				if(c > 0){
					nextPoint = pointLib.get(r).get(c-1);
					paths.add(nextPoint);
					w = true;
					//system.out.println("West validMove: " + nextPoint.toString());
				}
				
				if(w == false){
					//invalidAccess = new Point(negInf,negInf);
					paths.add(invalidAccess);
					//System.out.println("West validMove: " + invalidAccess.toString());
				}
				obj = new PointPaths(staticPoint, paths);
				pathHori.add(obj);
			}
			pathLib.add(r,pathHori);
		}
		
		
		
		return pathLib;
	}
	
	  // generate the maze starting from lower left
    private void generate() {
        generate(1, 1);     
    }
	
	public  ArrayList<ArrayList<PointPaths>> getMap2(){
		init();
		generate();
		
		ArrayList<ArrayList<Point>> pointLib = new ArrayList<ArrayList<Point>>();
		ArrayList<ArrayList<PointPaths>> pathLib = new ArrayList<ArrayList<PointPaths>>();
		
		
		int negInf = -Integer.MAX_VALUE;
		for(int r = 0; r < row; ++r){
			ArrayList<Point> hori = new ArrayList<Point>();
			for(int c = 0; c < column; ++c){
				////system.out.println("it is working");
				Point p = new Point(r,c);
				////system.out.println("Creating: " + p.toString());
				hori.add(p);
			}
			pointLib.add(r,hori);
			////system.out.println("r =" + r);
			
		}
		
		
		Boolean n = false;
		Boolean e = false;
		Boolean s = false;
		Boolean w = false;
		Point nextPoint; //neighbor point
		Point staticPoint; //the current point
		Point invalidAccess = new Point(negInf,negInf); //giving trash as an invalidAccess
		ArrayList<Point> paths;
		PointPaths obj;
		
		for(int r = 0; r < row; r++){
			ArrayList<PointPaths> pathHori = new ArrayList<PointPaths>();
			for(int c = 0; c < column; c++){
				//system.out.println("hmmm");
				
				paths= new ArrayList<Point>();
				staticPoint = pointLib.get(r).get(c);
				//Frequently update n, e, s, w to be false for conditions purposes
				if(north[c+1][r+1] == false && r > 0 ){
					nextPoint = pointLib.get(r-1).get(c);
					paths.add(nextPoint);
				} else {
					paths.add(invalidAccess);
				}
				
				if(east[c+1][r+1] == false && c < column - 1 ){
					nextPoint = pointLib.get(r).get(c+1);
					paths.add(nextPoint);
				} else {			
					paths.add(invalidAccess);
				}
				
				if(south[c+1][r+1] == false && r < row -1 ){
					nextPoint = pointLib.get(r+1).get(c);
					paths.add(nextPoint);
				} else {
					paths.add(invalidAccess);
				}
				
				if(west[c+1][r+1] == false && c >0){
					nextPoint = pointLib.get(r).get(c-1);
					paths.add(nextPoint);
				} else {
					paths.add(invalidAccess);
				}
				
				obj = new PointPaths(staticPoint, paths);
				pathHori.add(obj);
			}
			pathLib.add(r,pathHori);
		}
		
		
		
		return pathLib;
	}
	
	public ArrayList<ArrayList<PointPaths>> getMazeMap(){
		
		int currentX = 0; //make the current x coordinate to be zero as a starting coordinate
		int currentY = 0; //make the current y coordinate to be zero as a starting coordinate
		
		ArrayList<ArrayList<PointPaths>> map = new ArrayList<ArrayList<PointPaths>>(getMap2());
		
		return map;
		
	}
	
    private int N;                 // dimension of maze
    private boolean[][] north;     // is there a wall to north of cell i, j
    private boolean[][] east;
    private boolean[][] south;
    private boolean[][] west;
    private boolean[][] visited;
    private boolean done = false;
	
    private void init() {
        // initialize border cells as already visited
    	//N = column;
        visited = new boolean[column+2][row+2];
        for (int x = 0; x < column+2; x++) {
            visited[x][0] = true;
            visited[x][row+1] = true;
        }
        for (int y = 0; y < row+2; y++) {
            visited[0][y] = true;
            visited[column+1][y] = true;
        }


        // initialze all walls as present
        north = new boolean[column+2][row+2];
        east  = new boolean[column+2][row+2];
        south = new boolean[column+2][row+2];
        west  = new boolean[column+2][row+2];
        for (int x = 0; x < column+2; x++) {
            for (int y = 0; y < row+2; y++) {
                north[x][y] = true;
                east[x][y]  = true;
                south[x][y] = true;
                west[x][y]  = true;
            }
        }
    }
    
    
	// generate the maze
    private void generate(int x, int y) {
        visited[x][y] = true;

        // while there is an unvisited neighbor
        while (!visited[x][y+1] || !visited[x+1][y] || !visited[x][y-1] || !visited[x-1][y]) {

            // pick random neighbor (could use Knuth's trick instead)
            while (true) {
            	//Random rand = new Random();

            	//double r = StdRandom.uniform(4);
            	Random rand = new Random();

            	int  r = rand.nextInt(4);
            	
                if (r == 0 && !visited[x][y+1]) {
                    south[x][y] = false;
                    north[x][y+1] = false;
                    generate(x, y + 1);
                    break;
                }
                else if (r == 1 && !visited[x+1][y]) {
                    east[x][y] = false;
                    west[x+1][y] = false;
                    generate(x+1, y);
                    break;
                }
                else if (r == 2 && !visited[x][y-1]) {
                    north[x][y] = false;
                    south[x][y-1] = false;
                    generate(x, y-1);
                    break;
                }
                else if (r == 3 && !visited[x-1][y]) {
                    west[x][y] = false;
                    east[x-1][y] = false;
                    generate(x-1, y);
                    break;
                }
            }
        }
    }

	

	
}

