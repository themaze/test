import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GraphicGenerator extends JPanel {

	private int row = 10;
	private int column = 5;
	private final int mazeStartX = 0;
	private final int mazeStartY = 0;
	private final int pathSize = 34;
	private ArrayList<ArrayList<PointPaths>> map;
	private UserInterface request;
	
	// not implemented yet...
	private GridPoints maze;

	// direction for maze's neighbour
	private final int NORTH = 0;
	private final int EAST = 1;
	private final int SOUTH = 2;
	private final int WEST = 3;
	
	public GraphicGenerator(UserInterface request) {
		this.request = request;
		//playerPos = new Point(row/2,column/2);
		//prevMove = new Point(0,0);
		
		playerPos = new Point(0,0);
	
		maze = new GridPoints(row, column);
		map = maze.getMap2();
	}

	
	
	private Point playerPos;

	
	public void move(){
		int currX = playerPos.getX();
		int currY = playerPos.getY();
		
		if(islegalMove(request.getDx(), request.getDy(), currX, currY)){
			currX += (request.getDx());
			currY += (request.getDy());
			
			playerPos.setX(currX);
			playerPos.setY(currY);
		}	
		
	}
	
	private boolean islegalMove(int dx, int dy, int currX, int currY) {
		
//		System.out.println("X: " + currX);
//		System.out.println("Y: " + currY);
		
		if(currX + dx >= column || currY + dy >= row){
			return false;
		} else if(currX + dx < 0 || currY + dy < 0 ){
			return false;
		}

		PointPaths currAdj = map.get(currY).get(currX);
		ArrayList<Point> pts = currAdj.getPaths();
		
		int index = 0;
		
		if(dx == 0 && dy == 0){
			return false;
		} else if(dx == 0 && dy ==-1){
			index = 0;
		}  else if(dx == 1 && dy ==0){
			index = 1;
		}  else if(dx == 0 && dy == 1){
			index = 2;
		}  else if(dx == -1 && dy ==0){
			index  = 3;
		}
		
		int x = pts.get(index).getX();
		int y = pts.get(index).getY();
		if( Math.abs(x) == Integer.MAX_VALUE || Math.abs(y) == Integer.MAX_VALUE ){
//			System.out.println("inf");
//			System.out.println("curr Location and next index:" + currY + ", " + currY+ ", " +index);
//			System.out.println("next Location :" + pts.get(index).toString());
			return false;		
		}
		
		return true;
	}

	public void displayUser(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		
		//Color red = new Color(255, 0, 0);
		//g2.setColor(Color.RED);
		// JComponent dogComp = new JComponent();
		ImageIcon dog = new ImageIcon("res/dog.png"); 
		

		int currX = playerPos.getX();
		int currY = playerPos.getY();
		
		// dogComp.setSize(pathSize-3, pathSize-3);
		g.drawImage(dog.getImage(), currX*pathSize+ mazeStartX +1, currY*pathSize+mazeStartY+1, null);
		
		
		// creating player relative to the maze.
		//Rectangle player  = new Rectangle(currX*pathSize+ mazeStartX +1, currY*pathSize+mazeStartY+1, pathSize-3, pathSize-3);
		//g2.fill(player);
		//g2.draw(player);
	}
	

	public void displayMaze(Graphics g){
		
		Graphics2D g2 = (Graphics2D)g;
		Rectangle path = null;
		Line2D.Double wall = null;	
		
		for(int j = 0; j < row ; j++){
			for(int i = 0; i < column; i++){
				
				if(i < column ){
					// painting path
					g2.setColor(Color.WHITE);
					PointPaths currAdj = map.get(j).get(i);
					path = new Rectangle( i*pathSize+mazeStartX, j*pathSize+mazeStartY, pathSize, pathSize);
					g2.fill(path);
					g2.draw(path);
					
					ArrayList<Point> pts = currAdj.getPaths();

					// seeking for where is the wall exits
					for(int k = 0; k < 4; k++){
						int neighbourX = pts.get(k).getX();
						int neighbourY = pts.get(k).getY();
						
						// if neighbours are infinite they are the walls
						if( Math.abs(neighbourX) == Integer.MAX_VALUE || Math.abs(neighbourY) == Integer.MAX_VALUE ){
							
							// relative positions from where maze is generated
							int currJ = j*pathSize+mazeStartY;
							int currI = i*pathSize+mazeStartX;	
							int edgeJ = currJ+pathSize-1;
							int edgeI = currI+pathSize-1;
							
							// for debugging, the color will differentiate the direction of the wall
							if( k == NORTH ){
								g2.setColor(Color.BLACK);
								wall = new Line2D.Double(currI, currJ,  edgeI, currJ );
								g2.draw(wall);
								
							} else if( k == EAST) {
								g2.setColor(Color.BLACK);
								wall = new Line2D.Double(edgeI,currJ, edgeI, edgeJ );
								g2.draw(wall);
							}
							else if( k == SOUTH) {
								g2.setColor(Color.BLACK);
								wall = new Line2D.Double(currI,edgeJ, edgeI, edgeJ );
								g2.draw(wall);
							} else if( k == WEST) {
								g2.setColor(Color.BLACK);
								wall = new Line2D.Double(currI,currJ, currI, edgeJ );
								g2.draw(wall);
							}												
						} else{
							
						}
						
					}
				}
			}
		}
//		Color p = new Color(255, 255, 255);
//		g2.setColor(p);
		
//		wall = new Line2D.Double(mazeStartX, mazeStartY,  mazeStartX, pathSize*column + mazeStartY);
//		g2.draw(wall);
//		
//		wall = new Line2D.Double(mazeStartX, mazeStartY,  pathSize*row+mazeStartX, mazeStartY);
//		g2.draw(wall);
//		
//		wall = new Line2D.Double(mazeStartX, pathSize*column + mazeStartY,  pathSize*row+mazeStartX, pathSize*column + mazeStartY);
//		g2.draw(wall);
//		
//		wall = new Line2D.Double(pathSize*row+mazeStartX, mazeStartY,  pathSize*row+mazeStartX, pathSize*column + mazeStartY);
//		g2.draw(wall);
		
		
	}
	
	public void paintComponent(Graphics g){
		g.clearRect(0, 0, getWidth(), getHeight());
		
		
		Graphics2D g2 = (Graphics2D)g;
		Rectangle backGround = new Rectangle(0, 0, getWidth(), getHeight());
		g2.setColor(Color.BLACK);
		g2.fill(backGround);
		g2.draw(backGround);
		
		displayMaze(g);	
		displayUser(g);	
	}
}
