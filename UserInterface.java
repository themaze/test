import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;

public class UserInterface extends JPanel implements KeyListener{
	private int dx;
	private int dy;
	
    public UserInterface() {
        //this.setPreferredSize(new Dimension(500, 500));
        addKeyListener(this);
    }

    public void addNotify() {
        super.addNotify();
        requestFocus();
    }

    public void keyReleased(KeyEvent e) {
    	int key = e.getKeyCode();

	    if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
	        setDx(0);
	    } 

	    if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN ) {
	        setDy(0);
	    } 
    }
    public void keyTyped(KeyEvent e) {
    	
    }
    
    public void keyPressed(KeyEvent e) {

    	int key = e.getKeyCode();

	    if (key == KeyEvent.VK_LEFT) {
	        setDx(-1);
	    } else if (key == KeyEvent.VK_RIGHT) {
	        setDx(1);
	    } else {
	    	setDx(0);
	    }

	    if (key == KeyEvent.VK_UP) {
	        setDy(-1);
	    } else if (key == KeyEvent.VK_DOWN) {
	        setDy(1);
	    } else {
	    	setDy(0);
	    }
	    
	    //repaint();
	}


	public int getDx() {
		return dx;
	}


	public void setDx(int dx) {
		this.dx = dx;
	}


	public int getDy() {
		return dy;
	}


	public void setDy(int dy) {
		this.dy = dy;
	}
}
